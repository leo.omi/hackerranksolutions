(defn my-filter
  [f lst]
  (if (= lst '())
    (list)
    (let [fst (first lst)]
     (if (f fst)
      (conj (my-filter f (rest lst)) fst)
      (my-filter f (rest lst))))))

;; Filter implementation above was problematic with Hacker Rank's solution submission, so I adapted it to the solution below

(fn my-filter
  [delim lst]
  (if (= lst '())
    (list)
    (let [fst (first lst)]
     (if (> delim fst)
      (conj (my-filter delim (rest lst)) fst)
      (my-filter delim (rest lst))))))

