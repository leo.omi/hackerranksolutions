; Complete the countingValleys function below.
(defn doStep [result step]
  (println step)
)

(defn countingValleys [n s]
  (def steps (seq s))
  (reduce doStep {:level 0, :valleys 0} steps)
)

(def fptr (get (System/getenv) "OUTPUT_PATH"))

(def n (Integer/parseInt (clojure.string/trim (read-line))))

(def s (read-line))

(def result (countingValleys n s))

(spit fptr (str result "\n") :append true)
