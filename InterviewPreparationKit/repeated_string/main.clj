
(defn countChar [s c n]
  (let [substr (take n s)]
    (count
     (filter
      (fn [x] (= x c))
      substr)
     )
    )
  )

; Complete the repeatedString function below.
(defn repeatedString [s n]
  (let [len (count s)
        repetitions (long (/ n len))
        n_as (countChar s \a len)
        surplus (countChar s \a (mod n len))]
    (+ (* n_as repetitions)
       surplus)
    )
  )

(def fptr (get (System/getenv) "OUTPUT_PATH"))

(def s (read-line))

(def n (Long/parseLong (clojure.string/trim (read-line))))

(def result (repeatedString s n))

(spit fptr (str result "\n") :append true)
