(defn wordCount [words]
  (reduce
   (fn [hashMap word]
     (assoc hashMap word
            (inc (get hashMap word 0))))
   {} words))

(defn wordCheck [wordCount note]
  (let [currWord (nth note 0)
        noteCount (get wordCount currWord 0)]
    (if (empty? note)
      true
      (if (> noteCount 0)
        (and true
             (wordCheck
              (assoc wordCount currWord (dec noteCount))
              (take-last (dec (count note)) note)))
        false))))

(defn checkMagazine [magazine note]
  (if (wordCheck (wordCount magazine) note)
    (println "Yes")
    (println "No")))

(def mn (clojure.string/split (read-line) #" "))

(def m (Integer/parseInt (clojure.string/trim (nth mn 0))))

(def n (Integer/parseInt (clojure.string/trim (nth mn 1))))

(def magazine (clojure.string/split (read-line) #" "))

(def note (clojure.string/split (read-line) #" "))

(checkMagazine magazine note)
