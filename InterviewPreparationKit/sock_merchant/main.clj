(ns sock-merchant.core)

(defn incToNth [ar n]
  (update ar (dec n) inc))

(defn incPairs [result val]
  (+ result (int (/ val 2))))

(defn sockMerchant [n ar]
  (def zeroes (vec (repeat 100 0)))
  (def count (reduce incToNth zeroes ar))
  (reduce incPairs 0 count)
)

(def fptr (get (System/getenv) "OUTPUT_PATH"))

(def n (Integer/parseInt (clojure.string/trim (read-line))))

(def ar (vec (map #(Integer/parseInt %) (clojure.string/split (read-line) #" "))))

(def result (sockMerchant n ar))

(spit fptr (str result "\n") :append true)
