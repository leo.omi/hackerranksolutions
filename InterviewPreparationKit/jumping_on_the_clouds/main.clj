(defn cumulus? [clouds n]
  (if (>= n (count clouds))
    false
    (if (= (nth clouds n) 1)
      false
      true
      ))
  )

(defn minNotNegative [a b]
  (if (< a 0) b
      (if (< b 0) a
          (min a b)))
  )

(defn minJumps [clouds curr]
  (if (cumulus? clouds curr)
    (if (= (count clouds) (dec curr))
      0
      (inc (minNotNegative
            (minJumps clouds (+ curr 2))
            (minJumps clouds (+ curr 1))
            ))
      )
    -1
    )
  )

(defn jumpingOnClouds [c]
  (minJumps c 0)
  )

(def fptr (get (System/getenv) "OUTPUT_PATH"))

(def n (Integer/parseInt (clojure.string/trim (read-line))))

(def c (vec (map #(Integer/parseInt %) (clojure.string/split (read-line) #" "))))

(def result (jumpingOnClouds c))

(spit fptr (str result "\n") :append true)

